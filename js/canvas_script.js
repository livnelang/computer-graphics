/*
* On HTML5 DOM Load Function
*/
window.onload = function(){ 
    init();
}

/*
* Global Variables
*/
var canvas,
    context,
    dragging = false,
    dragStartLocation,
    snapshot,
    curve_pick = false,
    curveArray = [],
    shape,
    shape_element,
    colored_li,
    fillBox;

	 
    /*
    * Bresenaim Circle Function
    */
	function bres_circle ( x,  y,  radius) {
		var x_center = dragStartLocation.x, y_center = dragStartLocation.y;
		putPixel(x_center + x , y_center + y);
		putPixel(x_center - x , y_center + y);
		putPixel(x_center + x , y_center - y);
		putPixel(x_center - x , y_center - y);
		putPixel(x_center + y , y_center + x);
		putPixel(x_center - y , y_center + x);
		putPixel(x_center + y , y_center - x);
		putPixel(x_center - y , y_center - x);
	}

	/*
	* Oוur Basic PutPixel Function
	*/
	function putPixel(x, y) {
		context.fillRect(x, y, 1, 1);
	}


	/*
	* Draw A Circle Function
	*/
	function drawCircle(position) {
	 	//int radius = 50, x = 0, 
	 	var radius = Math.sqrt(Math.pow((dragStartLocation.x - position.x), 2) + Math.pow((dragStartLocation.y - position.y), 2));
	 	var y = radius, p ;
	 	var x = 0;
		p = 3 - 2*radius;
		bres_circle(x, y, radius);
		while(x < y) {
			bres_circle(x, y, radius);
			if(p < 0) {
				p = p + 4 * x + 6;
			}
			else {
				p = p + 4 * (x - y) + 10;
				y--;
			}
			x++;
		}
		while(x < y) {
			if(x == y) {
				bres_circle(x, y, radius);
			}
		}
	}

	/*
	* Draw A Bezier Curve
	*/
	function drawCurve(points) {
		console.log('inside drawCurve !');

		/* Bezier Points Calculations */
		var bezier = function(t, p0, p1, p2, p3){
		      	var dx =  p0.x,
					cx = -3 * p0.x +3 * p1.x,
					bx =  3 * p0.x -6 * p1.x +3 * p2.x,
					ax = -p0.x +3 * p1.x -3 * p2.x + p3.x;

				var dy =  p0.y,
					cy = -3 * p0.y +3 * p1.y,
					by =  3 * p0.y -6 * p1.y +3 * p2.y,
					ay = -p0.y +3 * p1.y -3 * p2.y + p3.y;	
		            
		      var x = (ax * Math.pow(t,3)) + (bx * Math.pow(t,2)) + (cx * t) + dx;
		      var y = (ay * Math.pow(t,3)) + (by * Math.pow(t,2)) + (cy * t) + dy;
		            
		      return {x: x, y: y};
    	}

    	  var accuracy = 0.1;
    	  dragStartLocation = points[0]; // Our Starting Point
    	  for (var i=0; i<1; i+=accuracy) {
	         var p = bezier(i, points[0], points[1], points[2], points[3]);

	         // self additions
	         drawLine(p);
	         dragStartLocation = p; 
	         //context.lineTo(p.x, p.y);
      		}
	}





	/*
	* Draw Line Function
	*/
	function drawLine(position) {
		var x_to = position.x, y_to = position.y ;
		var x_from  = dragStartLocation.x , y_from = dragStartLocation.y;

		// If we are going negative way
		
		if(x_to < x_from) {
			var temp_x = x_to;
			x_to = x_from;
			x_from = temp_x;
			var temp_y = y_to;
			y_to = y_from;
			y_from = temp_y;
		} 
		var dx = x_to - x_from;
		var dy = y_to - y_from;
		var error = 0;
		var derror = Math.abs(dy/dx);
		var y = y_from;
		for(var x = x_from; x < x_to; x++) {
			//var y = y_to + dy * (x - x_from) / dx;
			putPixel(x, y);
			error = error + derror;
			while(error >= 0.5) {
				putPixel(x, y);
				y = y + Math.sign(y_to - y_from);
				error = error - 1.0;
			}
		}
	}

	/*
	* Draw Polygon Function
	*/
	function drawPolygon(event) {
		context.save(); // saves the current context stage
		var radius, edges;
		dragStartLocation = getCanvasCoordinates(event); // Set The Middle Point
		/* We Need To Prompt Questions For : Radius, # Of Edges */
		radius = parseInt(prompt("Please enter the radius length"));
		if (radius != null) {
			edges = parseInt(prompt("Please enter number of edges"));
    	}
		
		 if (edges < 3) {
		  	return;
		  } 
		 var a = (Math.PI * 2)/edges;
		 var rotateAngle = -Math.PI/2;
		 context.beginPath();
		 context.translate(dragStartLocation.x,dragStartLocation.y);
		 context.rotate(rotateAngle);
		 context.moveTo(radius,0);
		 for (var i = 1; i < edges; i++) {
		    context.lineTo(radius*Math.cos(a*i),radius*Math.sin(a*i));
		 }
		 context.closePath();
		 context.stroke();

		 context.restore(); // Restores back the latest context stage
	  
}	

	/*
	* Function To Erase The Canvas Platform
	*/
	function eraseCanvas() {
				// Store the current transformation matrix
		context.save();

		// Use the identity matrix while clearing the canvas
		context.setTransform(1, 0, 0, 1, 0, 0);
		/*var canvas = document.getElementById("myCanvas");
		var context = canvas.getContext('2d'); */	
		context.clearRect(0, 0, canvas.width, canvas.height);
	}


/*
* TakeSnapshot
*/
function takeSnapshot() {
    snapshot = context.getImageData(0, 0, canvas.width, canvas.height);
}

/*
* RstoreSnapshot
*/
function restoreSnapshot() {
    context.putImageData(snapshot, 0, 0);
}

/*
* Getting Canvas Coordinates
*/ 
function getCanvasCoordinates(event) {
    var x = event.clientX - canvas.getBoundingClientRect().left,
        y = event.clientY - canvas.getBoundingClientRect().top;

    return {x: x, y: y};
}


/*
* Draw Switch Case Function
*/ 
function draw(position, shape) {
    if (shape === "circle") {
        drawCircle(position);
    }
    if (shape === "line") {
        drawLine(position);
    }
}

/*
*On Start Mouse Click
*/
function dragStart(event) {
    dragging = true;
    dragStartLocation = getCanvasCoordinates(event);
    takeSnapshot();
}

/*
* When Dragging The Mouse
*/ 
function drag(event) {
    var position;
    if (dragging === true) {
        restoreSnapshot();
        position = getCanvasCoordinates(event);
        //draw(position, "circle");
        draw(position, shape);

    }
}

/*
* When Releasing The Mouse
*/ 
function dragStop(event) {
    dragging = false;
    restoreSnapshot();
    var position = getCanvasCoordinates(event);
    draw(position, shape);


}

/*
* Used to determine whether the click is for: 
* 1. Bezier Curve
* 2. Regular Polygon
*/
function clickDetermine(event) {

	if(shape == "polygon") { // If its a polygon click
		console.log('polygon was clicked !');
		drawPolygon(event);
		return;
	}
	if (shape == "curve") {
		console.log("direct to curve");
		canvasClicked(event); // Direct to Bezier Curve Drawing Event
	}
	else {
		return;
	}
}

/*
* Used to collect points
*/
function canvasClicked(event) {
	dragStartLocation = getCanvasCoordinates(event);
	// Check if curve is picked
	if(curve_pick) {
		curveArray.push(dragStartLocation);
	}
	if(curveArray.length ==4) {
		console.log('4!');
		drawCurve(curveArray);
		curveArray = []; // zero the  coordinates array
	}

}

/* 
* Init Program Function, Takes part when DOM Is Done
*/
function init() {
    canvas = document.getElementById("myCanvas");
    context = canvas.getContext('2d');
    context.strokeStyle = 'green';
    context.fillStyle = 'red';
    context.lineWidth = 4;
    context.lineCap = 'round';

    /*
    * Set Selected Image <li> Opacity
    */
    function setOpacity(selected) {
    	/* Set Current Shape Opacity */
    	if(shape_element != null) {
    		shape_element.setAttribute("style", "opacity: 1;");
    	}
    	shape_element = selected;
    	shape_element.setAttribute("style", "opacity: 0.6;");
    }

    /*
    * Image Detection Triggers
    */
    document.getElementById("polygon_picture").addEventListener("click", function(){
		setOpacity(this);
     	shape = "polygon";
      });
    document.getElementById("curve_picture").addEventListener("click", function(){
		setOpacity(this);
		shape = "curve";
     	curve_pick = true;
     	console.log('curve clicked :' + curve_pick);
      });
    document.getElementById("line_picture").addEventListener("click", function(){
		setOpacity(this);
     	shape = "line";
      });
    document.getElementById("circle_picture").addEventListener("click", function(){
		setOpacity(this);
     	shape = "circle";
      });
     document.getElementById("polygon_picture").addEventListener("click", function(){
		setOpacity(this);
     	shape = "polygon";
      });


	/*
	* On Color Change Click
	*/
	$(".colors li").click(function(e){
		var color = $(this).css("background-color");
		context.fillStyle = color; 		// General Color
		context.strokeStyle = color;   // This one is for the Polygons 
		/* Return opacity of old colored li*/ 
		if(colored_li != null) {
			colored_li.style.opacity = 1;
		}
		colored_li = e.target;
		colored_li.style.opacity = 0.5;
	}); 


	/*
	* Mouse Event Listeners
	*/
    canvas.addEventListener('click', clickDetermine, false);
    canvas.addEventListener('mousedown', dragStart, false);
    canvas.addEventListener('mousemove', drag, false);
    canvas.addEventListener('mouseup', dragStop, false);
    

}



